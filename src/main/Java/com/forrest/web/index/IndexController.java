package com.forrest.web.index;

import com.jfinal.core.Controller;

/**
 * Created by forre on 2017/6/27.
 */
public class IndexController extends Controller {
    public void index() {
        render("login/index.html");
    }
}
