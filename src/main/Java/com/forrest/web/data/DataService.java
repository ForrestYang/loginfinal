package com.forrest.web.data;

import com.forrest.web.common.model.User;

import java.util.List;

public class DataService {

    private static final User userDao = new User().dao();

    public List<User> display() {
        return userDao.find("select * from l_user order by id asc");
    }

    public List<User> search(String search) {
        return userDao.find("select * from l_user where username = ? or pwd = ?", search, search);
    }

    public void delete(int id) {
        userDao.deleteById(id);
    }

    public User findById(int id) {
        return userDao.findById(id);
    }

    public boolean updatehead(String headName) {
        return new User().setHeadsculp(headName).update();
    }

}
