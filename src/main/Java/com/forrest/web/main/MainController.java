package com.forrest.web.main;

import com.jfinal.core.Controller;

public class MainController extends Controller {
    public void index() {
        render("index.html");
    }
}
