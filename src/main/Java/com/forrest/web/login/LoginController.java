package com.forrest.web.login;

import com.jfinal.captcha.CaptchaRender;
import com.jfinal.core.Controller;
import com.jfinal.kit.Ret;

/**
 * Created by forre on 2017/6/27.
 */
public class LoginController extends Controller {
    private static final String RANDOM_CODE_KEY = "1";
    LgRgService lgRgService = new LgRgService();

    public void index() {

//        String method = getRequest().getMethod();
//        if (method.equalsIgnoreCase("GET")) {
//            render("index.html");
//            return;
//        }
        String name = getPara("username");
        String password = getPara("password");
        String inputRandomCode = getPara("captcha");
        Ret ret = new Ret();
        boolean rc = CaptchaRender.validate(this, inputRandomCode.toUpperCase());
        if (!rc) {
            ret = Ret.by("status", false).set("message", "验证码错误");
        } else if (!lgRgService.exist(name)) {
            ret = Ret.by("status", false).set("message", "用户不存在");
        } else if (!lgRgService.check(name, password)) {
            ret = Ret.by("status", false).set("message", "密码错误");
        } else {
            ret = Ret.by("status", true);
        }
        renderJson(ret);
    }

    public void captcha() {
        renderCaptcha();
    }
}
