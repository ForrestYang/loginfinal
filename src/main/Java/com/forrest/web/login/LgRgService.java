package com.forrest.web.login;

import com.forrest.web.common.model.User;

public class LgRgService {

    private static final User userDao = new User().dao();

    public boolean check(String name, String password) {
        User user = userDao.findFirst("select * from l_user where username=? and pwd=?", name, password);
        return null != user;
    }

    public boolean exist(String name) {
        User user = userDao.findFirst("select * from l_user where username=?", name);
        return null != user;
    }

    public boolean save(String name, String password, String psdagain) {
        return new User().setUsername(name).setPwd(password).setSalt(psdagain).save();
    }

}
