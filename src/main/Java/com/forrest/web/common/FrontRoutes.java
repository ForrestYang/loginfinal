package com.forrest.web.common;

import com.forrest.web.data.DataController;
import com.forrest.web.index.IndexController;
import com.forrest.web.login.LoginController;
import com.forrest.web.main.MainController;
import com.forrest.web.register.RegisterController;
import com.jfinal.config.Routes;

public class FrontRoutes extends Routes {
    public void config() {

        setBaseViewPath("_view");
        add("/", IndexController.class);
        add("/login", LoginController.class);
        add("/register", RegisterController.class);

        add("/data", DataController.class);
        add("/main", MainController.class);

    }
}
