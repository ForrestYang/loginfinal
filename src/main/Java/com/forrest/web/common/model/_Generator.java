package com.forrest.web.common.model;

import com.forrest.web.common.BaseConfig;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

public class _Generator {

    public static void main(String[] args) {

        String baseModelPackageName = "com.forrest.web.common.model.base";
        String baseModelOutputDir = "src/main/java/com/forrest/web/common/model/base";
        String modelPackageName = "com.forrest.web.common.model";
        String modelOutputDir = "src/main/java/com/forrest/web/common/model";

        DruidPlugin dp = new BaseConfig().getDruidPlugin();
        dp.start();

        Generator generator = new Generator(dp.getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);

        generator.setDialect(new MysqlDialect());
        generator.setGenerateChainSetter(true);
        generator.setGenerateDaoInModel(false);
        generator.setRemovedTableNamePrefixes("l_");
        generator.setGenerateDataDictionary(false);

        generator.generate();

        dp.stop();

    }

}