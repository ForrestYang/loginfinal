package com.forrest.web.common;

import com.forrest.web.common.model._MappingKit;
import com.jfinal.config.*;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

public class BaseConfig extends JFinalConfig {


    private static final Prop p = PropKit.use("a_little_config.txt");

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 80, "/");
    }

    public void configConstant(Constants me) {
        me.setDevMode(PropKit.getBoolean("devMode", true));
        me.setBaseUploadPath(p.get("baseUploadPath"));
        me.setBaseDownloadPath("/");
    }

    public void configHandler(Handlers me) {
    }

    public void configInterceptor(Interceptors me) {
    }

    public void configRoute(Routes me) {
        me.add(new FrontRoutes());
    }

    public void configEngine(Engine me) {
        me.addSharedFunction("/_view/common/_layout.html");
        me.addSharedFunction("/_view/common/_nav.html");
    }

    public DruidPlugin getDruidPlugin() {
        return new DruidPlugin(p.get("jdbcUrl"),
                p.get("user"), p.get("password"));
    }

    public void configPlugin(Plugins me) {
        DruidPlugin dp = getDruidPlugin();
        me.add(dp);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        me.add(arp);
        _MappingKit.mapping(arp);
    }

    @Override
    public void afterJFinalStart() {

    }
}
