package com.forrest.web.register;

import com.forrest.web.login.LgRgService;
import com.jfinal.core.Controller;

/**
 * Created by forre on 2017/6/27.
 */
public class RegisterController extends Controller {
    LgRgService lgRgService = new LgRgService();

    public void index() {

        String method = getRequest().getMethod();
        if (method.equalsIgnoreCase("GET")) {
            render("index.html");
            return;
        }

        String name = getPara("name");
        String password = getPara("password");
        String psdagain = getPara("psdagain");
        if (lgRgService.exist(name)) {
            renderText("用户名已存在");
        } else if (!password.equals(psdagain)) {
            renderText("两次密码不匹配");
        } else if (lgRgService.save(name, password, psdagain)) {
            renderText("注册成功！");
        } else {
            renderText("系统异常");
        }
    }
}
