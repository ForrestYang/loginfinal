layui.use(['form', 'jquery', 'layer'], function () {
    var form = layui.form();
    var $ = layui.jquery;
    var layer = layui.layer;

    form.on('submit(submitLogin)', function (data) {
        var postData = data.field;
        $.ajax({
            url: '/login',
            method: 'post',
            data: data.field,
            success: function (loginData) {
                console.log(loginData);
                console.log(loginData.status);
                console.log(loginData.message);
                if (!loginData.status) {
                    layer.open({
                        title: '1'
                        , content: loginData.message
                    });
                    updateCaptcha();
                } else {
                    location.pathname = '/main';
                }
            },
            error: function () {
                layer.open({
                    title: '2'
                    , content: '未知错误'
                });
            }
        });
        return false;
    });


    function updateCaptcha() {
        $('#captcha-img').prop('src', 'login/captcha?v=' + Math.random());
    }

    $('#captcha-img').click(function () {
        updateCaptcha();
    });
});
